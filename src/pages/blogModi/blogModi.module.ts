import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogChangePage } from './blogModi';

@NgModule({
  declarations: [
    BlogChangePage,
  ],
  imports: [
    IonicPageModule.forChild(BlogChangePage),
  ],
})
export class BlogChangePageModule {}
