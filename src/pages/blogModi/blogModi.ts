import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import {Blog, BlogProvider} from "../../providers/blog/blog";
/**
 * Generated class for the BlogChangePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-change',
  templateUrl: 'blogModi.html',
})
export class BlogChangePage {
  public pk: number;
  public title: string;
  public body: string;
  public imageUrl: String;
  private blogItem: Blog;

  constructor(public navCtrl: NavController, public navParams: NavParams, private blogData: BlogProvider, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlogChangePage');
      this.pk = this.navParams.get('pk');
      this.title = this.navParams.get('title');
      this.body = this.navParams.get('body');
      this.imageUrl = this.navParams.get('imageUrl')
  }

  private changeBlogPost() {

    this.blogItem = {
      id: this.pk,
      title: this.title,
      body: this.body
    }

    if (!this.title || !this.body) {
      let alert = this.alertCtrl.create({
        title: 'No way!',
        subTitle: 'Blogtitel und Text muss eingegeben werden!',
        buttons: ['OK']
      });
      alert.present();
    } else {

      this.blogData.update(this.blogItem).subscribe(result => {
        console.log('updated', result);
        this.navCtrl.popToRoot()
      })

    }

  }


}
