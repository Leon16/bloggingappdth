import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Blog, BlogProvider} from "../../providers/blog/blog";
import {Observable} from "rxjs";
import {BlogDetailPage} from "../blogDetail/blogDetail";
import {BlogNewPage} from "../blogNeu/blogNeu";
import {tap} from "rxjs/operators";

/**
 * Generated class for the BlogListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-list',
  templateUrl: 'blogList.html',
})
export class BlogListPage {
  public blogs$: Observable<Blog[]>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private blogData: BlogProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlogListPage');

    this.loadBlogs();
  }

  ionViewDidEnter() {
    this.loadBlogs();
  }

  private loadBlogs() {

    this.blogs$ = this.blogData.getBlogs.pipe(
      tap(data => console.log(data))
    );
  }

  public goToDetail(pk: number | string, imageUrl: string, title: string, body: string) {
    this.navCtrl.push(BlogDetailPage, {pk: pk, imageUrl: imageUrl, title: title, body: body});
  }

  public goToNewBlog() {
    this.navCtrl.push(BlogNewPage)
  }

  public removeBlog(pk: number | string) {
    let alert = this.alertCtrl.create({
      title: 'Löschen',
      subTitle: 'Willst du den Datensatz ' + pk + ' wirklich löschen?',
      buttons: ['Dismiss', {
        text: 'Yes',
        handler: () => {
          this.blogData.remove(pk).subscribe(result => {
            console.log('removed', result);
            this.loadBlogs();
          })
        }
      }]
    });
    alert.present( );


  }
}
