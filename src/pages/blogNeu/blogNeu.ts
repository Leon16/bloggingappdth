import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import {Blog, BlogProvider} from "../../providers/blog/blog";
import {Observable} from "../../../node_modules/rxjs";
import { BlogListPage} from "../blogList/blogList";

/**
 * Generated class for the BlogNewPage page.
 */
@IonicPage()
@Component({
  selector: 'page-blog-new',
  templateUrl: 'blogNeu.html',
})
export class BlogNewPage {
  newBlogPost: Blog;
  titleString: string;
  bodyString: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private blogData: BlogProvider,
              private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlogNewPage');
  }
  private submitNewBlogPost(){
    if (!this.titleString || !this.bodyString) {
      let alert = this.alertCtrl.create({
        title: 'No way!',
        subTitle: 'Du must einen Titel und Text definieren!',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.newBlogPost =
        {
          id: null,
          title: this.titleString,
          body: this.bodyString
        }
        console.log(this.newBlogPost)
      this.blogData.create(this.newBlogPost).subscribe(result => {
        console.log('created', result);
        this.navCtrl.push(BlogListPage)
      })
    }
  }

}
