import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogNewPage } from './blogNeu';

@NgModule({
  declarations: [
    BlogNewPage,
  ],
  imports: [
    IonicPageModule.forChild(BlogNewPage),
  ],
})
export class BlogNewPageModule {}
