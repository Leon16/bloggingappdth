import { Component } from '@angular/core';
import { ToastController ,AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Observable} from "rxjs";
import {Blog, BlogProvider} from "../../providers/blog/blog";
import {Comment, CommentProvider} from "../../providers/comment/comment";
import {BlogChangePage} from "../blogModi/blogModi";
import {BlogListPage} from "../blogList/blogList";

/**
 * Generated class for the BlogDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-detail',
  templateUrl: 'blogDetail.html',
})
export class BlogDetailPage {
  public pk: number | string;
  public imageUrl: String;
  public title: string;
  public body: string;
  private newComment: Comment;
  private newCommentBody: string;
  private newCommentPostID: number | string;


  public blog$: Observable<Blog>;
  public comments$: Observable<Comment[]>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private blogData: BlogProvider,
              private commentData: CommentProvider,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    this.pk = this.navParams.get('pk');
    this.imageUrl = this.navParams.get('imageUrl');
    this.title = this.navParams.get('title');
    this.body = this.navParams.get('body');
    this.blog$ = this.blogData.getBlog(this.pk);

    this.comments$ = this.commentData.getCommentByFilter('blog='+this.pk);

    console.log('ionViewDidLoad BlogDetailPage');
  }

  private changeCurrentPost() {
    this.navCtrl.push(BlogChangePage, {pk: this.pk, title: this.title, body: this.body, imageUrl: this.imageUrl})
  }

  addCommentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Neuen Komentar verfassen',
      inputs: [
        {
          name: 'comment',
          placeholder: 'Kommentar...'
        }
      ],
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Posten',
          handler: data => {
            if (data.comment) {
              // create comment
              this.newCommentBody = data.comment;
              this.newCommentPostID = this.pk;

              this.newComment = {
                id: null,
                blog: this.newCommentPostID,
                body: this.newCommentBody,
              }

              this.postNewComment();
              this.presentToastNewComment('Kommentar erfolgreicht erstellt');
              this.updateComments();

            } else {
              // invalid input
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  postNewComment() {
    this.commentData.create(this.newComment).subscribe(result => {
      console.log('created', result);
    })
  }

  presentToastNewComment(commentString: string) {
    let toast = this.toastCtrl.create({
      message: commentString,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  deleteComment(commendID: number){
    this.commentData.remove(commendID).subscribe(result => {
      console.log('removed comment', result);
      this.presentToastNewComment('Kommentar gelöscht')
      this.updateComments()
    })
  }

  public removeBlog(pk: number | string) {
    let alert = this.alertCtrl.create({
      title: 'Löschen',
      subTitle: 'Willst du den Post: "' + this.title + '" wirklich löschen?',
      buttons: ['Nein', {
        text: 'Ja',
        handler: () => {
          this.blogData.remove(pk).subscribe(result => {
            console.log('removed', result);
            this.navCtrl.push(BlogListPage);
          })
        }
      }]
    });
    alert.present( );
  }

  updateComments() {
    this.comments$ = this.commentData.getCommentByFilter('blog='+this.pk);
  }
}
