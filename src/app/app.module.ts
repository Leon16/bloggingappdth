import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {BlogListPage} from "../pages/blogList/blogList";
import {BlogDetailPage} from "../pages/blogDetail/blogDetail";
import {BlogNewPage} from "../pages/blogNeu/blogNeu";
import {BlogChangePage} from "../pages/blogModi/blogModi";
import { BlogProvider } from '../providers/blog/blog';
import {HttpClientModule} from "@angular/common/http";
import { CommentProvider } from '../providers/comment/comment';
import {FormsModule} from "@angular/forms";


// Rest API Server URL
export const BASE_URL = 'https://blogs.ymjproductions.ch/api/v1';

@NgModule({
  declarations: [
    MyApp,
    BlogListPage,
    BlogDetailPage,
    BlogNewPage,
    BlogChangePage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    FormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BlogListPage,
    BlogDetailPage,
    BlogNewPage,
    BlogChangePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BlogProvider,
    CommentProvider
  ]
})
export class AppModule {}
